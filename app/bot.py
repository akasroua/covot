from datetime import datetime

from telethon import TelegramClient, events
from telethon.sessions import StringSession

from app.notifier import send_mail
from constants import API_HASH, API_ID, BOT_TOKEN, EXAMPLE, SESSION
from database.crud import *

bot = TelegramClient(StringSession(SESSION), API_ID, API_HASH).start(
    bot_token=BOT_TOKEN
)


@bot.on(events.NewMessage(pattern="/start"))
async def conversation(event):
    entity = event.sender_id
    async with bot.conversation(entity=entity) as conv:
        await conv.send_message(
            "Hola, muy buenas. Gracias por ponerte en contacto con UGR Tracing Bot. Para poder continuar con el proceso por favor responda a este mensaje con un identificador único (DNI o correo institucional). Muchas gracias."
        )
        unique_id = await conv.get_response()
        user = search_database(id=unique_id.text.lower())
        send_mail(recipient=user.correo_institucional)
        await event.respond(
            "El usuario se ha identificado con éxito. Se ha mandado una clave alfanumérica a su correo institucional. Por favor envíe la clave para continuar con el proceso."
        )
        verification_code = await conv.get_response()
        valid_code = verify_code(
            id=user.numero_de_documento, code=verification_code.text
        )
        if not valid_code:
            await event.respond("La clave que indica no es correcta")
            conv.cancel()
        await event.respond(
            f"""Hola {user.nombre}. Por favor seleccione su tipo de vivienda:
            1. Familiar
            2. Piso compartido
            3. Piso individual
            4. Residencia de estudiantes o Colegio Mayor
            5. Otra
            """
        )
        accomodation_type = await conv.get_response()
        save_attribute(
            id=user.numero_de_documento,
            attribute="tipo_alojamiento",
            data=accomodation_type.text,
        )
        await event.respond(
            """Por favor seleccione su tipo de caso COVID-19:
            1. Confirmado
            2. Contacto estrecho
            3. Sospechoso
            """
        )
        case_type = await conv.get_response()
        save_attribute(
            id=user.numero_de_documento,
            attribute="tipo_caso",
            data=case_type.text,
        )
        await event.respond("¿Tiene síntomas compatibles con el COVID-19?")
        symptoms = await conv.get_response()
        parsed_symptoms = parse_boolean_string(value=symptoms.text.lower())
        save_attribute(
            id=user.numero_de_documento,
            attribute="sintomas",
            data=parsed_symptoms,
        )
        await event.respond("Indique la fecha de inicio del aislamiento.")
        quarantine_start = await conv.get_response()
        save_attribute(
            id=user.numero_de_documento,
            attribute="comienzo_aislamiento",
            data=datetime.strptime(quarantine_start.text, "%d/%m/%Y"),
        )
        await event.respond("Indique la fecha de fin del aislamiento.")
        quarantine_end = await conv.get_response()
        save_attribute(
            id=user.numero_de_documento,
            attribute="fin_aislamiento",
            data=datetime.strptime(quarantine_end.text, "%d/%m/%Y"),
        )
        await event.respond(
            f"""Por favor confirme que los siguientes datos son correctos:
            1. Nombre: {user.nombre}
            2. Apellidos: {user.primer_apellido} {user.segundo_apellido}
            3. Documento identificativo: {user.numero_de_documento}
            4. Tipo de domicilio: {user.tipo_alojamiento  }
            5. Tipo de caso COVID-19: {user.tipo_caso}
            6. Síntomas COVID-19: {boolean_to_string(user.sintomas)}
            7. Periodo de aislamiento: {user.comienzo_aislamiento} - {user.fin_aislamiento}
            """
        )
        confirmation = await conv.get_response()
        parsed_confirmation = parse_boolean_string(confirmation.text.lower())
        if not parsed_confirmation:
            await event.respond("Un saludo")
            conv.cancel()
        await event.respond(
            "Su caso ha sido comunicado correctamente. Muchas gracias por utilizar UGR Tracing Bot. Le deseamos una pronta recuperación"
        )


if __name__ == "__main__":
    create_database(data=EXAMPLE)
    bot.run_until_disconnected()
