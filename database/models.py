from sqlalchemy import Column
from sqlalchemy.types import Boolean, Date, Integer, String, Text

from database import Base


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, autoincrement=True)
    primer_apellido = Column(String, nullable=False)
    segundo_apellido = Column(String, nullable=False)
    nombre = Column(String, nullable=False)
    tipo_de_documento = Column(String, nullable=False)
    numero_de_documento = Column(String, nullable=False, unique=True)
    centro_academico = Column(String, nullable=False)
    correo_institucional = Column(String, nullable=False, unique=True)
    tipo_alojamiento = Column(Integer)
    tipo_caso = Column(Integer)
    sintomas = Column(Boolean)
    comienzo_aislamiento = Column(Date)
    fin_aislamiento = Column(Date)
    observaciones = Column(Text)
    codigo = Column(String)
