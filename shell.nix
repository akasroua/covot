{ pkgs ? import <nixpkgs> { } }:

with pkgs;

mkShell {
  buildInputs =
    [ python38 python38Packages.telethon python38Packages.sqlalchemy sqlite ];

}
