# Covot

Covid tracing bot project for the Universidad of Granada.

## Setup

The project is developed using the [Nix](https://nixos.org/) package manager. Follow the instructions on the website linked above to install it.

You only need to enter a nix-shell to acquire the dependencies:

``` sh
nix-shell
```

## Usage

The bot can be executed using the following command:

```sh
python -m app.bot
```

